# README #


### What is this repository for? ###

This repository houses data files and code associated with Toczydlowski et al. 2021. Poor data stewardship will hinder global genetic diversity surveillance. PNAS.

### How do I get set up? ###

* Clone the repository to your local computer.
* If you use RStudio, double click the .Rproj file at the top level of the repo.
* Run the scripts you want to.
* If you don't use RStudio, open the scripts you would like to run as you do other R scripts and edit any file paths in them accordingly (the path as written are recursive within the repository).


### Who do I talk to? ###

* Rachel Toczydlowski, rhtoczyd@msu.edu, https://orcid.org/0000-0002-8141-2036
* Eric Crandall, eric.d.crandall@gmail.com, https://orcid.org/0000-0001-8580-3651